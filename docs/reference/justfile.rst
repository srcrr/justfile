justfile
========

.. testsetup::

    from justfile import *

.. automodule:: justfile
    :members:

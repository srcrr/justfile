=====
Usage
=====

Reading From a File
===================

Reading is pretty simple:

	>> from justfile import read
	>> content = read("myfile.txt")
	>> print(content)
	Hello World!
	
If the file doesn't exist or we couldn't read the result will be None::

	>> from justfile import read
	>> content = read("notfound.txt")
	>> print(content)
	None

There is the option (``complain``) to raise an exception on error::
		
	>>> from justfile.io import read
	>>> content = read('somefile.txt', complain=True)
	Traceback (most recent call last):
	  File "<stdin>", line 1, in <module>
	  File "justfile/io.py", line 27, in read
		raise e
	  File "justfile/io.py", line 19, in read
		raise JustFileException(f"{path} does not exist.")
	justfile.exception.JustFileException: somefile.txt does not exist.

Want something besides ``None`` returned?

	>>> from justfile.io import read
	>>> content = read('somefile.txt', default_content="Hello")
	>>> print(content)
	Hello

That's about it.

Writing to a File
=================

Just as easy as reading from a file. If the path doesn't exit it will
simiply be created::

	>>> from justfile.io import read
	>>> from justfile.io import write
	>>> write("test.txt", "Hello, World!")
	True
	>>> read("test.txt")
	'Hello, World!'



from unittest import TestCase
from justfile.io import read, write, append
import os
import shutil

class TestIo(TestCase):
	
	def setUp(self):
		self.ok = "./path/to/ok.txt"
		self.badperm = "./path/to/badperm.txt"
		self.dne = "./doesnotexist/path/to/dne.txt"
		self.message = "this is a test"
		self.newfile = "./path/tonew/file.txt"
		self.newfile2 = "./path/to/new/file.txt"
		
		if not os.path.exists(os.path.dirname(self.ok)):
		    os.makedirs(os.path.dirname(self.ok))
		
		with open(self.ok, 'w+') as f:
			f.write(self.message)
		
		with open(self.badperm, 'w+') as f:
			f.write(self.message)
		
		os.chmod(self.badperm, 0)
	
	def tearDown(self):
		shutil.rmtree('./path')
	
	def test_read(self):
		self.assertEqual(read(self.ok), self.message)

	def test_read_default(self):
		self.assertEqual(read(self.dne, default_content="failsafe"), "failsafe")

	def test_read_complain(self):
		with self.assertRaises(Exception):
			self.assertEqual(read(self.badperm, complain=True))
	
	def test_write_ok(self):
		self.assertEqual(write(self.newfile, self.message, complain=True), True)
		with open(self.newfile, 'r') as f:
			self.assertEqual(f.read(), self.message)

	def test_write_complain(self):
		with self.assertRaises(Exception):
		    self.assertEqual(write(self.badperm, self.message, complain=True), True)

